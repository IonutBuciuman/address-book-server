'use strict';

var fs = require('fs');

var APIController = {};

APIController.getInfo = function getInfo(request, response, next) {
	
	var packageJson = fs.readFile('package.json', 'utf8', function readFileCb(error, file) {
		var info = {};
		
		if (error) {
			next(error);
		} else {
			file = JSON.parse(file);
			
			info.name = file.name;
			info.version = file.version;
			info.description = file.description;
			info.author = file.author;
			
			info.endpoints = {
				"/contacts/": "GET, POST",
				"/contacts/:id": "GET, PATCH, DELETE",
				"____________________________________": "",
				"API_Description": {
					"Contact": {
						"payload": {
							"name": "Sherlock Holmes",
							"phoneNumber": "0740123456",
							"address": "Baker's Street, 221th",
							"description": "World's Greatest Detective",
							"email": "sherlock_holmes@privateditective.com"
						},
						"fields": {
							"name": "required",
							"phoneNumber": "required",
							"address": "optional",
							"description": "optional",
							"email": "optional"
						}
					}
				}
			};
			
			response.send(info);
		}
	});
};

module.exports = APIController;
