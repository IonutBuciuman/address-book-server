'use strict';

var ContactsService = require('server/services/contacts-service');

var AddressBookController = {};

AddressBookController.getContacts = function (request, response, error) {
	ContactsService.getContacts().then(function (result) {
		response.status(200).json(result);
	}, function (msg) {
		response.status(404).json(msg);
	});
};

AddressBookController.getContact = function (request, response, error) {
	var id = request.params.id;
	
	ContactsService.getContact(id).then(function (result) {
		response.status(200).json(result);
	}, function (msg) {
		response.status(404).json(msg);
	});
};

AddressBookController.createContact = function (request, response, error) {
	var payload = request.body;
	
	ContactsService.createContact(payload).then(function (result) {
		response.status(200).json(result);
	}, function (msg) {
		response.status(404).json(msg);
	});
};

AddressBookController.updateContact = function (request, response, error) {
	var id = request.params.id;
	var payload = request.body;
	
	ContactsService.updateContact(id, payload).then(function (result) {
		response.status(200).json(result);
	}, function (msg) {
		response.status(404).json(msg);
	});
};

AddressBookController.deleteContact = function (request, response, error) {
	var id = request.params.id;
	
	ContactsService.deleteContact(id).then(function (result) {
		response.status(200).json(result);
	}, function (msg) {
		response.status(404).json(msg);
	});
};

module.exports = AddressBookController;