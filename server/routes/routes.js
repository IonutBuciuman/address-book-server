'use strict';

var APIController = require('server/controllers/api-controller');
var AddressBookController = require('server/controllers/address-book-controller');

module.exports = function router(express) {
	
	var router = new express.Router();
	
	router.route('/')
			.get(APIController.getInfo);
	
	router.route('/contacts')
			.get(AddressBookController.getContacts)
			.post(AddressBookController.createContact);
	
	router.route('/contacts/:id')
			.get(AddressBookController.getContact)
			.patch(AddressBookController.updateContact)
			.delete(AddressBookController.deleteContact);
	
	return router;
};


