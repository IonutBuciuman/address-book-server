'use strict';

module.exports = function(code, message) {
	
	var error = new Error();
	error.code = code;
	error.message = message;
	
	return error;
};
