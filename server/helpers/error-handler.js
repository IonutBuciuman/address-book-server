'use strict';

module.exports = function errorHandler(error, req, res, next) {
	if (error) {
		switch (error.code) {
			case 11000:
				res.status(409)
					.json({
						code: 'CONFLICT',
						message: 'Document already exists'
					});
				return;
			
			case 400:
				res.status(400)
					.json({
						code: 'BAD_REQUEST',
						message: error.message
					});
				return;
			
			case 404:
				res.status(404)
					.json({
						code: 'NOT_FOUND',
						message: error.message || 'Document does not exist'
					});
				return;
			
			default:
				return next(error);
		}
	} else {
		next();
	}
};
