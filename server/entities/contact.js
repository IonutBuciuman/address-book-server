'use strict';

var mongoose = require('mongoose');
var monguurl = require('monguurl');
var Schema = mongoose.Schema;

var ContactSchema = new Schema({
	name: {
		type: String,
		required: true
	},
	phoneNumber: {
		type: String,
		required: true
	},
	email: {
		type: String,
		required: false
	},
	description: {
		type: String,
		required: false
	},
	address: {
		type: String,
		required: false
	},
	created: {
		type: Number,
		default: new Date().getTime()
	}
});

module.exports = mongoose.model('Contact', ContactSchema);