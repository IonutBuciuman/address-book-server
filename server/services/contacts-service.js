'use strict';

var Contact = require('server/entities/contact');

var ContactsService = {};
var options = {
	__v: 0
	// _id: 0
};

ContactsService.getContacts = function () {
	var query = {};
	
	return new Promise(function(resolve, reject) {
		Contact.find(query, options, function (error, contacts) {
			if (error) {
				reject(error);
			} else {
				resolve(contacts);
			}
		});
	});
};

ContactsService.getContact = function (id) {
	var query = {
		_id: id
	};
	
	return new Promise(function(resolve, reject) {
		Contact.findOne(query, options, function (error, contact) {
			if (error) {
				reject(error);
			} else {
				if (contact) {
					resolve(contact);
				} else {
					reject("nothing to find");
				}
			}
		});
	});
};

ContactsService.createContact = function (data) {

	return new Promise(function(resolve, reject) {
		Contact.create(data, function (error, contact) {
			if (error) {
				reject(error);
			} else {
				delete contact.__v;
				resolve(contact);
			}
		});
	});
};

ContactsService.updateContact = function (id, data) {
	var query = {
		_id: id
	};
	
	return new Promise(function(resolve, reject) {
		Contact.findOneAndUpdate(query, data, options, function (error, contact) {
			if (error) {
				reject(error);
			} else {
				if (contact) {
					resolve(contact);
				} else {
					reject("nothing to patch... ");
				}
			}
		});
	});
};

ContactsService.deleteContact = function (id) {
	var query = {
		_id: id
	};
	
	return new Promise(function(resolve, reject) {
		Contact.findOne(query, options, function (error, contact) {
			if (error) {
				reject(error);
			} else {
				if (contact) {
					contact.remove();
					resolve(contact);
				} else {
					reject("nothing to delete");
				}
			}
		});
	});
};

module.exports = ContactsService;
