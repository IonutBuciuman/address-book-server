'use strict';

require('app-module-path').addPath(__dirname);

var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var environmentConfig = require('server/config/environment-config');
var routes = require('server/routes/routes')(express);
var errorHandler = require('server/helpers/error-handler');
var app = express();

/**
 * Connecting to the MongoDB DataBase
 * @returns {Promise}
 */
function establishConnectionDB() {
	return new Promise(function (resolve, reject) {
		mongoose.connect(environmentConfig.database, function mongooseConnectionCb(error) {
			if (error) {
				reject(error);
			} else {
				resolve({
					success: true,
					message: 'Connected to the database: ' + environmentConfig.database
				});
			}
		});
	});
}

/**
 * Adding middlewares and Endpoints routing to Express
 * @param app
 */
function initialize(app) {
	
	/**
	 * Enable CROSS
	 */
	app.use(function (req, res, next) {
		res.header("Access-Control-Allow-Origin", "*");
		res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, PATCH, DELETE");
		res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
		next();
	});
	
	/**
	 * Adding Express MiddleWares
	 */
	app.use(bodyParser.urlencoded({
		extended: true
	}));
	
	app.use(bodyParser.json());
	
	/**
	 * 	API routes
	 */
	app.use('', routes);
	
	/**
	 * Error handler
	 */
	app.use(errorHandler);
}

/**
 * Starting Express server
 * @param app
 */
function startServer(app) {
	app.listen(environmentConfig.port, function () {
		console.info('Server is running on: ' + environmentConfig.url + ':' + environmentConfig.port);
	});
}

/**
 * Bootstrapping the application
 */
establishConnectionDB()
    .then(function (response) {
        console.info(response.message);
        initialize(app);
    })
    .then(function () {
        startServer(app);
    })
    .catch(function (error) {
        console.error(error);
    });